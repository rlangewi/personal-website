---
layout: post
title: My Top 25 Board Games of All Time - 2016
date: 2016-08-30
displaydate: August 30 2016
---
Those that know me well know that I have quite the interest in tabletop board games. Truth is, I really have a fascination with games and game design in general, and board games have the huge advantage of combining those game design elements with the enjoyment of being able to share in that experience with friends. Many of my fondest memories spent with people have been found sitting around a table enjoying a game with friends and/or family. I believe it is truly an exciting time to be in the hobby of board games, as every year there are tons of new designs being released that continue to push the medium and provide new and unique ways to provide a fun and interactive experience.
<br>
<br>
Every year or so, I find it interesting to rank my top board games to see how they stack up and what has changed from the previous year. For purposes of this list, I limit the selection to games that I currently own. There are a few games I don't own that probably would have made the list, but it is much simpler for me to evaluate if I only consider games that I have and play regularly. So without further ado, here is the countdown for this year!

#25 - Incan Gold
--------------
![Image](/images/posts/3/incangold.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
3-8 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;20 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$16.99
{% endraw %}
</p>
<br>
[Incan Gold][incangold] is a very simple game, but one that scales really well for larger groups and provides an entertaining experience. The only decision you make in the game is around a simple push-your-luck mechanic: do you go deeper into the temple, with hopes that you will find more treasure and get larger portions due to other players dropping out? Or do you save what treasure you have, hoping that riskier players end up losing it all? So simple and short, but one that I always find myself enjoying while we are playing.

#24 - Dixit
--------------
![Image](/images/posts/3/dixit.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
3-6(12) Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$34.99
{% endraw %}
</p>
<br>
[Dixit][dixit] is another game that goes over well with larger groups. I have several versions/expansions combined together, but I especially recommend the Dixit: Odyssey set as it expands the game up to 12 players. The basic mechanic of coming up with clues that are neither obvious nor too ambiguous for weird dream-like illustrations provides a surprising amount of depth and replayability. Many players find it stressful to come up with clues, but the majority of the game is spent guessing other players' clues anyway. Looking at all the available cards to try and identify the one that matches the clue is very satisfying, and is why Dixit makes my list.

#23 - Carcassonne
-----------------
![Image](/images/posts/3/carcassonne.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-5(6) Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30-45 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$22.99
{% endraw %}
</p>
<br>
[Carcassonne][carcassonne] is somewhat of a "classic" in terms of modern board games, but it is one I still enjoy quite a bit. I should give the disclaimer that I own both the [Inns and Cathedrals][innsandcathedrals] and [Traders and Builders][tradersandbuilders] expansions, which are fairly essential in my opinion, and the game would probably not rank as high for me without them. The game is very relaxing to play, simply drawing new tiles and trying to figure out ways to play them to maximize points, but still has a fair amount of strategy to keep things interesting. Additionally, the way the map starts with one tile and grows over the duration of the game is cool to see on the table. An oldie, but one that still gets a recommendation from me.

#22 - Sushi Go Party!
---------------------
![Image](/images/posts/3/sushigoparty.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-8 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;20 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$19.99
{% endraw %}
</p>
<br>
About a year ago, I purchased the original Sushi Go! with hopes that it would be a good casual game that could be enjoyed by anyone. It fit the niche perfectly, with the only drawback being that it only supported 5 players. Recently, [Sushi Go Party!][sushigoparty] was released, expanding the game up to 8 players. Additionally, it adds a lot of new cards that change the deck from game to game, as well as a scoring board so that pen and paper is no longer necessary. At its core, the game takes the drafting mechanic found in games like [7 Wonders][7wonders] and distills it down to its simplest form. Just pick a card, play it, and pass the rest to the next player. With no downtime due to players playing simultaneously, this is a game that works well with just about anyone, and for that reason I consider it a must-have in any game collection.

#21 - Rampage (Terror in Meeple City)
-------------------------------------
![Image](/images/posts/3/rampage.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;45-60 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$44.99
{% endraw %}
</p>
<br>
[Rampage][rampage] (now sold as Terror in Meeple City) is certainly one of the more unique games in my collection. It is the kind of game that really packs that "wow" factor when you show it to new people, simply because there isn't really anything else like it. The game has you literally knocking down the game components as you destroy the miniature city, while still having some interesting strategic depth in how you approach your destruction. The game is just fun, and is short enough to not outstay its welcome.

#20 - Dead of Winter
--------------------
![Image](/images/posts/3/deadofwinter.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-5 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;60-180 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$39.99
{% endraw %}
</p>
<br>
[Dead of Winter][deadofwinter] is a game that falls into the category of "semi-cooperative" where the players are working together against the game, but there is the possibility of a traitor that secretly wants to sabotage the team's efforts. The game expands on this concept with the interesting addition of giving each player a secret objective in addition to the main objective. In order to win the game, players not only need to win as a team, but also need to complete their secret goal. This create all kinds of interesting scenarios where people are trying to distinguish between suspicious activity that could be a traitor, but could also just be a player that needs to take tangential actions in order to pursue their objective. Add on a post-apocalyptic zombie survival theme, and you have a very intriguing social experience.

#19 - Space Cadets
------------------
![Image](/images/posts/3/spacecadets.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
3-6 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;90-120 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$39.99
{% endraw %}
</p>
<br>
[Space Cadets][spacecadets] is the first fully cooperative game to make the list, and it is another game that has some pretty unique elements to make it stand apart in my collection. The main things that distinguish the game are that each player is controlling a different portion of the ship that all have different gameplay mechanisms, and all of the main gameplay is done in real-time against the clock. This makes for some high-stress team efforts that can be an absolute blast with the right group of people. After the timed portion is over, you get to see the results of the players' actions unfold. The shields officer may have shielded the front of the ship, only to find that the player at the helm miscalculated and now the ship is facing backwards! And the weapons officer may have loaded three torpedos to fire that turn, only to find that the sensors officer failed to acquire the target locks necessary to kill the two enemies in close proximity. The interplay of all these different stations makes for a very fun cooperative experience.

#18 - Codenames
---------------
![Image](/images/posts/3/codenames.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
4-8 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;20 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$14.99
{% endraw %}
</p>
<br>
[Codenames][codenames] made quite the splash in the board game scene about a year ago, and rightly so; it is one of the most original and fun party game concepts that I have played in a long time. The spy master for each team has quite the task in trying to come up with one word clues that will help their team identify their team's words, while avoiding the other team's words as well as the assassin who causes an instant game loss. Certainly a brain burner for those giving clues, but very fascinating. On the other side, players try to figure out what clues could mean and push their luck with how many words they guess. I find this to be a very elegant game design from a great designer (Vlaada Chvatil), and it is one that I enjoy quite a bit.

#17 - 7 Wonders
---------------
![Image](/images/posts/3/7wonders.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
3-7 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$39.99
{% endraw %}
</p>
<br>
[7 Wonders][7wonders] can be thought of as a bit more of a "gamer's" version of Sushi Go Party. While still an excellent introductory game for those new to the hobby, it takes the drafting mechanism and adds a little more meat on the bones in terms of strategy and variety. The great thing about a game like 7 Wonders is how smoothly it scales up to 7 players without extending the game length at all. A game that can provide a strategically satisfying experience for up to 7 players (8 with the Cities expansion) in just a half hour is certainly a rarity, and it is still a game I love even after many, many plays.

#16 - Ghost Stories
-------------------
![Image](/images/posts/3/ghoststories.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;45-60 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$37.99
{% endraw %}
</p>
<br>
[Ghost Stories][ghoststories] is one of the first cooperative games that I ever played. With its extreme difficulty and large amount of luck, it honestly seems like it shouldn't rank this high for me. But yet, time after time, Ghost Stories delivers solid and memorable cooperative experiences. It always seems that we are overcoming the most desperate of situations, and it so often comes down to just one roll of the dice that was set up by the most unlikely chain of events. The more I have played cooperative games, the more I have learned how important it is for them to be difficult in order to supply that satisfaction when you do succeed in winning the game; Ghost Stories has that satisfying difficulty in spades.

#15 - Dominion
--------------
![Image](/images/posts/3/dominion.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$29.99
{% endraw %}
</p>
<br>
[Dominion][dominion] is one of the games in my collection that I have owned the longest. Even after buying so many new and exciting games, it still manages to stand the test of time. I can't think of many games that have single-handedly invented a new genre the way that Dominion did with deck-building (that is, deck-building that happens as part of the gameplay as opposed to beforehand). It plays quick, and every game is fresh with a new set of ten kingdom cards to choose from. It's hard to say if I will ever get tired of playing it, because if I would, I feel like it would have happened already.

#14 - The Castles of Burgundy
-----------------------------
![Image](/images/posts/3/castlesofburgundy.png)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;90 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$26.99
{% endraw %}
</p>
<br>
Despite having possibly the most dull cover of any game in my collection, [The Castles of Burgundy][castlesofburgundy] is a fantastic game in which you take actions using die rolls in order to place land and building tiles onto your player board. While it may still not sound all that exciting, it is rich with tactical decision making and opportunities to chain together combos that feel oh so satisfying. While a stronger theme would maybe help it hit the table more often, it always provides a great experience when we pull it out.

#13 - The Lord of the Rings: The Card Game
------------------------------------------
![Image](/images/posts/3/lordoftherings.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
1-4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30-60 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$29.99
{% endraw %}
</p>
<br>
[The Lord of the Rings: The Card Game][lordoftherings] takes the deck-building, collectable card game formula found in games like Magic: The Gathering, removes the randomized purchases using Fantasy Flight's Living Card Game model, and turns it into a cooperative experience. While I make no effort to put my wallet under the stress that it takes to keep up with all the new content for the game, the core set and few expansions I have provide plenty of replay value even if I don't make another purchase. I enjoy this best as a 2 player experience, where you work together with decks that complement each other to try and complete a given scenario. For me, it scratches that itch of a collectable card game, without needing to sink a bunch of money into it or find people that want to play head to head.

#12 - PitchCar
--------------
![Image](/images/posts/3/pitchcar.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-8 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;60 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$64.99
{% endraw %}
</p>
<br>
[PitchCar][pitchcar] almost doesn't classify as a board game, as it is much closer to an experience like shuffleboard. That said, it plays on a table top and gets packed in a box, so that is close enough for me! Flicking discs around a modular racetrack is just pure, unadulterated fun.

#11 - Robinson Crusoe: Adventures on the Cursed Island
------------------------------------------------------
![Image](/images/posts/3/robinsoncrusoe.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
1-4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;90-120 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$64.99
{% endraw %}
</p>
<br>
I am a sucker for cooperative games, and [Robinson Crusoe: Adventures on the Cursed Island][robinsoncrusoe] is another great one. Huge amount of replayability due to the various scenarios, randomized decks of events cards, and different available inventions used in each game. Definitely has the potential to suffer from the "alpha player" problem where one player can tell everyone else what to do, but with a good group this is one of the best cooperative games out there.

#10 - Mage Knight: The Board Game
---------------------------------
![Image](/images/posts/3/mageknight.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
1-4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180-300 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$55.99
{% endraw %}
</p>
<br>
[Mage Knight][mageknight] is probably the longest game in my collection, clocking it at around 3-5 hours depending on the number of players. Despite its length, the game has me engaged the whole time and usually we are surprised when we see how long we have been playing. The rules are certainly complex, but it is extremely satisfying to use clever card play to battle enemies on the map, and then level up and gain new cards and abilities. More than any other game in my collection, this is one where I don't really care much about the final scoring. Everyone kind of just enjoys accomplishing their own goals, and tallying points at the end is more of an afterthought. This is a strength in my mind, because even the losers go away from the game feeling like it was a worthwhile experience. Now hopefully we just play it often enough that I can remember all the rules...

#9 - Escape: The Curse of the Temple
------------------------------------
![Image](/images/posts/3/escape.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-5(6) Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;15 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$39.99
{% endraw %}
</p>
<br>
Talk about a game that couldn't be more drastically different than Mage Knight... [Escape][escape] plays in just 15 minutes in real-time with almost no strategy to be found. Yet it ranks this high on my list because it is just so FUN. Everyone is furiously rolling their dice and coordinating as they explore the temple, and it continues pedal-to-the-metal until the game is over a mere 10 minutes later. While the base game is great, adding in the expansions [Escape: Illusions][escapeillusions] and [Escape: Quest][escapequest] takes the replay value and variety to another level. I consider this a must own in my collection, simply because of the unique niche that only it can fill.

#8 - Dungeon Fighter
--------------------
![Image](/images/posts/3/dungeonfighter.png)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-6 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30-45 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$32.99
{% endraw %}
</p>
<br>
[Dungeon Fighter][dungeonfighter] comes in as my favorite dexterity game, and one that I think is grossly underrated. I think many people brush it off as extremely casual and silly, and rightly so; you are literally throwing dice onto a target with game effects that may alter the throw into various trick shots. That said, my core gaming group has played this upwards of 60 times, and we approach it very much as a "gamer's" party game. There is more skill involved than initially meets the eye, and it is fun to ramp up the difficulty and see if the group can handle it. The addition of expansions (which I have two of four so far) really spices things up with new content and mechanics, and it contributes to why Dungeon Fighter is probably my biggest darkhorse purchase of all time.

#7 - Tichu
----------
![Image](/images/posts/3/tichu.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;60 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$9.99
{% endraw %}
</p>
<br>
With all of the complex designs that come out every year and those that I own and love, it is amazing that a game like [Tichu][tichu] can rank so highly for me. Tichu is hands down the best traditional-style card game I have ever played. The deck is nearly a standard deck, with the addition of 4 unique special cards, and some added flavor in the changing of the suits. The depth of strategy in how to play your hand, how to coordinate with your partner without knowing their hand, and not knowing whether other players have certain special cards or a bomb (a straight flush or four of a kind that can beat any other card)... I have no doubt that I will never tire of this game. At just $9.99 for a box with two decks, you owe it to yourself to pick up a copy if you enjoy strategic card games (e.g. Hearts, Spades, Bridge, etc.).

#6 - Race for the Galaxy
------------------------
![Image](/images/posts/3/raceforthegalaxy.png)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-4 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;15-30 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$22.99
{% endraw %}
</p>
<br>
[Race for the Galaxy][raceforthegalaxy] is my most played game of all time. Almost all of those plays are two player with my brother, as it became our go-to game for quick late-night strategy without being too much of a brain burner. The game definitely has a high barrier to entry with all the iconography (and throwing in all 3 expansions probably doesn't help), but for players that know the game really well, it is the best "gamer's" filler that I can think of. We can knock out 2-3 games in an hour or less, and yet we feel like we have made so many meaningful, strategic decisions in that short time. Any game that can be played over 300 times and still feel fresh has to rank highly on my list.

#5 - Cosmic Encounter
---------------------
![Image](/images/posts/3/cosmicencounter.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
3-5(8) Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;60 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$44.99
{% endraw %}
</p>
<br>
[Cosmic Encounter][cosmicencounter] is the source of some of my most memorable moments in gaming. More than any other game on my list, what makes this game amazing is what happens outside of the game itself, between the players. So much of the game is around the interplay of the players as they form temporary alliances and jostle to put themselves in a position to win. The game is unique in that any number of players could win, provided that they all reach five foreign colonies at the same moment. This adds such an interesting dynamic, and is at its best when all players are willing to share a victory instead of losing, but greedy enough to try and win by oneself if they think it is possible. The variety and replay value is through the roof with all of the alien powers that will be different in every game. Throw in 5 expansions, and you have a lifetime of gaming possibilities best enjoyed with repeat plays within the same group.

#4 - Twilight Struggle
----------------------
![Image](/images/posts/3/twilightstruggle.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$41.99
{% endraw %}
</p>
<br>
For the longest time when I was first getting into the hobby, [Twilight Struggle][twilightstruggle] held the number one spot on [Board Game Geek][boardgamegeek], the authority on board games. With credentials like that, I was eventually tempted enough to buy it, even though it wasn't really the type of game that seemed like it would be a good fit for us. Man am I glad I pulled the trigger on it, because it has to the be the best 2 player experiences of any game I've played. The tension that rises out of the game mechanics is nothing short of brilliant. Instead of your typical point scoring, Twilight Struggle uses a "tug-of-war" mechanic where each player pulls the marker closer to their side whenever they score. This means the game could end at any time if a player can pull it far enough in their direction. On top of that, players are manipulating the ever-growing number of regions to try and swing control in their favor, but the cards that score the regions could come up at any time. I could get carried away in my description with how many aspects of this game are fantastic from a game design perspective, so I'll just leave it with this: Twilight Struggle is one of the top-rated games of all time for good reason.

#3 - Terra Mystica
------------------
![Image](/images/posts/3/terramystica.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-5 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;60-150 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$52.99
{% endraw %}
</p>
<br>
There are some games that give a good idea of how they feel to play simply by reading the rules or descriptions on paper. For [Terra Mystica][terramystica], it wasn't until I actually played it that I grasped how good this game felt to play. Don't get me wrong, the mechanics and game design on paper are fantastic and I knew it was going to be a good game. That said, it is one of those games where it is hard to explain why players are enjoying themselves so much the whole time. It is probably a combination of really good short term goals that give players direction, as well as a wealth of "good" options that make you feel that you are always making good progress, but whatever it is, I can't deny how much I enjoy it relative to other games.

#2 - Eldritch Horror
--------------------
![Image](/images/posts/3/eldritchhorror.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
1-8 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;180 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$44.99
{% endraw %}
</p>
<br>
My gaming group loves cooperative games, and [Eldritch Horror][eldritchhorror] is far and away my favorite of the genre. For years we played and loved [Arkham Horror][arkhamhorror], which would have probably made my top 10 games of all time in its own right. But for me, Eldritch Horror takes everything I loved about Arkham Horror, streamlines it, and expands the formula in exciting and satisfying ways. The sheer variety in the different characters, enemies, items, and events in each game always keep it fresh, and the Cthulhu theme packs the experience with so much flavor. It has that perfect balance of strategic game mechanics and randomized events such that there is a tangible excitement through the game as the players wonder what will happen next. Similar to [Cosmic Encounter][cosmicencounter], Arkham Horror was a game that provided very memorable moments that we would talk about long after the game ended, and I am excited to carry on that tradition with many plays of Eldritch Horror.

#1 - Agricola
-------------
![Image](/images/posts/3/agricola.jpg)

<p style="text-align: center; font-weight: bold;">
{% raw %}
2-5 Players&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;60-150 Minutes&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$45.99
{% endraw %}
</p>
<br>
[Agricola][agricola] has been my favorite game for around 5 years. Whenever I have the opportunity to play it, I find myself thinking, "is this really still my favorite game?" And every single time, the playthrough answers with a resounding "YES!" I am well aware that I am predisposed to like most dry, strategic "eurogames" simply because I tend to place much more emphasis on game mechanics and I enjoy thinking through systems of resources and strategic/tactical decisions. For me, what elevates Agricola up and above other great strategy games is the cards that are dealt out at the beginning of each game. Looking at a hand of 14 cards that you have never seen together before, and forming a long-term strategy of how you plan to achieve victory is simply a rush of adrenaline. Then throughout the game, those initial plans inevitably get hindered and you find yourself making quick tactical audibles to try and adjust your long-term strategy. At the end of the game, you get to finally exhale and see how everything came together. I'll be the first to admit Agricola can be stressful, but I absolutely adore that tense battle against the odds, and the game reliably delivers my favorite experience of any board game.

The End
-------
Thanks for reading! I plan to post an updated list annually, so it will be interesting to see how games shift around and which new games might work their way up the list. I've also thought about posting more board game related content on here, whether about games I have played or game design in general. Until then, happy gaming!

[incangold]: https://boardgamegeek.com/boardgame/37759/incan-gold 
[dixit]: https://boardgamegeek.com/boardgame/92828/dixit-odyssey
[carcassonne]: https://boardgamegeek.com/boardgame/822/carcassonne 
[innsandcathedrals]: https://boardgamegeek.com/boardgameexpansion/2993/carcassonne-expansion-1-inns-cathedrals
[tradersandbuilders]: https://boardgamegeek.com/boardgameexpansion/5405/carcassonne-expansion-2-traders-builders
[sushigoparty]: https://boardgamegeek.com/boardgame/192291/sushi-go-party
[7wonders]: https://boardgamegeek.com/boardgame/68448/7-wonders 
[rampage]: https://boardgamegeek.com/boardgame/97903/terror-meeple-city 
[deadofwinter]: https://boardgamegeek.com/boardgame/150376/dead-winter-crossroads-game
[spacecadets]: https://boardgamegeek.com/boardgame/123096/space-cadets
[codenames]: https://boardgamegeek.com/boardgame/178900/codenames 
[ghoststories]: https://boardgamegeek.com/boardgame/37046/ghost-stories
[dominion]: https://boardgamegeek.com/boardgame/36218/dominion
[castlesofburgundy]: https://boardgamegeek.com/boardgame/84876/castles-burgundy
[lordoftherings]: https://boardgamegeek.com/boardgame/77423/lord-rings-card-game
[pitchcar]: https://boardgamegeek.com/boardgame/150/pitchcar
[robinsoncrusoe]: https://boardgamegeek.com/boardgame/121921/robinson-crusoe-adventures-cursed-island
[mageknight]: https://boardgamegeek.com/boardgame/96848/mage-knight-board-game
[escape]: https://boardgamegeek.com/boardgame/113294/escape-curse-temple
[escapeillusions]: https://boardgamegeek.com/boardgameexpansion/128236/escape-illusions
[escapequest]: https://boardgamegeek.com/boardgameexpansion/137405/escape-quest
[dungeonfighter]: https://boardgamegeek.com/boardgame/102548/dungeon-fighter
[tichu]: https://boardgamegeek.com/boardgame/215/tichu
[raceforthegalaxy]: https://boardgamegeek.com/boardgame/28143/race-galaxy
[cosmicencounter]: https://boardgamegeek.com/boardgame/39463/cosmic-encounter
[twilightstruggle]: https://boardgamegeek.com/boardgame/12333/twilight-struggle
[terramystica]: https://boardgamegeek.com/boardgame/120677/terra-mystica
[eldritchhorror]: https://boardgamegeek.com/boardgame/146021/eldritch-horror
[arkhamhorror]: https://boardgamegeek.com/boardgame/15987/arkham-horror
[agricola]: https://boardgamegeek.com/boardgame/31260/agricola
[boardgamegeek]: https://boardgamegeek.com/
