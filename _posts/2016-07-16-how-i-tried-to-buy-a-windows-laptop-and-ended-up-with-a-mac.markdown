---
layout: post
title:  "How I Tried to Buy a Windows Laptop and Ended Up with a Mac"
date:   2016-07-16
displaydate: July 16 2016
categories: website
---
Towards the end of 2015, I was finishing up my Master's degree in Computer Science at Colorado School of Mines. With graduation and the end of my school career quickly approaching, it seemed like a good time to invest in a new personal laptop. Up to this point, I had always been a Windows user, and had used a Dell Latitude laptop all the way through college. I never had any huge loyalties to Windows or Microsoft, but it had always served me well as an effective tool for my hobbies, so I felt no reason to switch. Naturally, I researched to find the top Windows laptops on the market. Many reviews later I settled on the [Dell XPS 13][dellxps13] and went ahead and placed an order through Dell's website. Now I am typing up this blog post on my MacBook Pro. What happened?
<br>
<br>

![Image](/images/posts/2/dell-xps-13.jpg)

Chapter 1: A Birthday Spoiled - 12/04/15
------------------------------------------
My family and I gathered around the table in celebration of my 23rd birthday. There were few presents to be opened, as I had requested that funds be contributed towards my new laptop. Having received the machine in the mail a few days earlier, I brought it down and showed it off. It looked slick and way sexier than the clunky business laptop that I had been using. What better gift could I receive than a new tool to fuel the hobbies I was most passionate about? I was already planning projects that I would be able to start on once I wrapped up my Master's thesis. This should have been the end of the story.

### A Problem Surfaces
It was exciting to set up a new laptop for the first time in 5 years. Starting from scratch, I was able to really streamline and organize things effectively and get rid of a lot of the bloat that had collected on my old machine. It did not take long for me to notice that something wasn't quite right, though. I think at first I tried to write it off as a fluke or blame myself.
<br>
<br>
"That was weird, I guess my thumb hit the spacebar twice."
<br>
<br>
"Surely there wouldn't be a problem with my brand new, highly-rated laptop, right?"
<br>
<br>
But alas, there was clearly a problem. Occasionally when I hit the space bar, it would register more than once, inserting multiple spaces into the text. Writing an email required me to constantly delete backwards to get rid of duplicate spaces. Even worse trying to code. A minor defect that rendered the laptop unusable for me.
<br>
<br>
That's alright though, these things happen. I'll simply contact Dell and get this sorted out. I got in contact with a support agent through their online chat, and worked with him to figure out the next steps. Some troubleshooting indicated that it was a hardware problem, and would require me to send it in for repair. A fairly painless interaction, albeit a little impersonal as the agent clearly picked from his range of pre-defined responses. No worries, it sounds like it will just be a bit of an inconvenience and then it will be fixed.
<br>
<br>

![Image](/images/posts/2/bsod.png)

Chapter 2: A Christmas Spoiled - 12/15/15
--------------------------------------------------
With my Master's thesis finished, and my full-time position at [ReadyTalk][readytalk] not starting until January, it was the first time I had a month free of work and school since 4 years earlier. I had a lot of ideas around how I was going to use the time productively to work on creative projects that I hadn't had time to start. Unfortunately for me, most of those projects required a laptop.
<br>
<br>
On December 15th, I shipped the Dell XPS 13 back to their repair facility in a box that they provided in the mail. I was notified that they received the system on December 18th, and I received it back on December 25th (Merry Christmas). I did not have a lot of time to test the laptop out on Christmas Day, but I was able to at least verify that the space bar problem was fixed! This should have been the end of the story.

### Trading One Problem for Another
I think it is a fair for one to expect that sending a laptop in for repair should leave it in a better state than it was previously. Unfortunately, this does not describe my experience in this case. Upon setting up and configuring Windows (for the 2nd time), I was prompted that Windows 10 needed to update. Sure, why not? I let it do its thing, and tried to get back to installing various programs. Except now the internet didn't work.
<br>
<br>
Of course, this must be a problem with our home Wi-Fi, we have problems like this from time to time. But a quick check indicates that the internet is working in the rest of the house. Upon closer investigation, the computer indicated that the network hardware was not found.
<br>
<br>
"Dang, the update must have screwed something up with the drivers."
<br>
<br>
I went ahead and ran the Dell diagnostics through the BIOS to try and get more information. This yielded the lovely error, "Hard Drive - Not Installed". Um, what? My extensive five minute Google search clarified that this was not a good thing. Time to go ahead and contact my good friend Rendy from Dell customer support.

### We've Talked Before, You Know
I typed up a nice detailed response and sent it to the same contact that I had originally for the spacebar issue. What I got back was nearly identical to the templated response that I had received earlier, giving me instructions on how to send the machine in to Dell's repair facility. I responded with something along the lines of:
<br>
<br>
"I'm not sure if you recall, but I just got this machine back after the 10-day process of sending it in for repair, I'm not exactly jumping up and down to send it again..."
<br>
<br>
At this point I was convinced my machine was just a dud (the model did get great reviews, after all), so I requested that I could be sent a replacement system. Instead of good old Rendy responding to this request, he passed me along to another agent who gave me this response:
<br>
<br>
"This is to confirm that we have received your email. Your email has been placed in queue and we will get back to you as soon as possible. We appreciate your patience in this matter. I want to assure you that we place a strong emphasis on ownership for all customer issues and that your issue will be resolved soonest."
<br>
<br>
Wait, so I got placed in a queue, but my issue will be resolved soonest? That is an interesting claim to make to all the people you send that copy-and-pasted response. Happy New Year.
<br>
<br>

![Image](/images/posts/2/surfacebook.jpg)

Chapter 3: Rising to the Surface - 1/25/16
------------------------------------------
It turns out that getting a replacement system from Dell isn't the quickest process in the world. During this waiting period, I had the opportunity to think about everything that had happened so far, realize how frustrating it was that I had gone about a month and a half without a working laptop, and second guess myself on what steps to take next. In the time that had passed since I originally ordered the Dell XPS 13, I had some additional money saved. I had also had some thoughts on how a tablet might be useful, particularly for my budding interest in classical piano composition (being able to sit at the piano with a tablet that could notate and then play it back was an attractive concept). This led me down the rabbit hole of researching the then new Microsoft Surface Book. With various reviews tauting it as one of the best (albeit expensive) Windows laptops, it seemed like a solid option given that it would be less expensive than buying a laptop and a tablet separately. So with a replacement Dell XPS 13 on the way (albeit slowly), I made the decision to pursue the Surface Book.
<br>
<br>
My first step was contacting Dell to let them know that I was planning on returning the replacement system. It wasn't an ideal situation (would have been nice to decide to switch before requesting a replacement), but I figured I had some leverage given my negative experience thus far, and I could send back the replacement brand new without opening it. The Dell agent replied asking that I still open and test the replacement computer, but could then request a return. With that confirmation (or so I thought, see Chapter 7) that I could get a refund from Dell, and my pressing desire to acquire a computer to continue my projects that had been on hold for 2 months, I acted quickly to buy a Surface Book.

### My First Trip to the Mall
The previous two months had really soured my experience with buying laptops online, so I was happy to take advantage of the fact that we have a Microsoft outlet store at one of the local malls. My [fiancee Mary][mary] and I headed over there with the intention to buy, barring any drastic issues discovered while testing the in-store sample machines. Everything went smoothly and we purchased the Surface Book. Ironically, the same day (January 25th), the replacement machine from Dell finally came in the mail. This should have been the end of the story. I should have returned the replacement Dell laptop and lived on happily with the Surface Book. But of course things could never go that smoothly, right?
<br>
<br>

![Image](/images/posts/2/guestlogin.jpg)

### A Surface's Problems
Upon arriving at home and unpacking the Surface Book, I ran into my first issue. After powering on the laptop, instead of it going into the normal Windows 10 setup (which I was quite familiar with at this point from the Dell XPS 13 saga), it simply went to a login screen for some "guest user" that required a password.
<br>
<br>
"Uhhh... I don't have a password..."
<br>
<br>
After some online research, I was able to find a way to hard reset the Surface Book and cause it to properly initiate the Windows 10 setup. Apparently several Surface Books had this issue due to the factory setup not being completed properly, and I (being the broken technology magnet I am), managed to find one of them. No worries though, it was a solvable issue, and we were up and running now.
<br>
<br>
For a delicate couple of days, it appeared that everything was fine with the Surface Book. I was a big fan of the higher quality keyboard (compared to the Dell XPS 13) and the beautiful, large screen. I was a little underwhelmed with the reliability of the tablet detaching mechanism (struck me very much as a work-in-progress prototype), but nothing about it was broken (a nice change of pace). I even was able to use the Surface Book to play the newly released puzzle game [The Witness][witness], which I highly recommend. But no, fate wasn't finished with me yet, and I quickly became aware of an annoying problem. Essentially, whenever I would be browsing the internet and would scroll through content that had associated audio (i.e. videos in my Facebook timeline), the speakers would "crackle", almost as if they were preparing themselves in case I would actually start the video. With the abundance of embedded video content in webpages these days, let me just say that this eats away at the soul of someone who just dropped two grand on a laptop. I really had no choice, despite unconsciously denying it for a few weeks; I needed to return it.

### My Second Trip to the Mall
Still convinced that the Surface Book was a good option for me (and assuming I just got a dud), I headed back to the mall with Mary. I explained the issues I was having to the employees there (they seemed confused), but they obliged me and set me up with a replacement machine. One huge advantage of the physical store was being able to instantly exchange and have a new laptop, rather than waiting God only knows how many days for Dell to ship things back and forth. Being the eternal optimist that I am, I headed home convinced that I had finally reached the end of my quest for a laptop.
<br>
<br>

![Image](/images/posts/2/microsoft-store.jpg)

### Scratch That
Opening the package to the new Surface Book, I prepared myself for what would now be the *fifth* time going through the Windows 10 setup process (this includes the replacement Dell XPS 13 for all of you who are counting). Naturally, I ran into the exact same issue as the first Surface Book where the initial boot-up led to some unknown guest login. Having researched the solution to it once already, I quickly applied the hard reset to start the actual Windows 10 installation. My subconscious opinions of Microsoft and Windows were certainly not enhanced by my two-for-two track record of the installation process not working properly on what was supposed to be their "ultimate laptop".
<br>
<br>
With the setup complete, my first order of business was to verify that I could no longer replicate the sound crackling issue. Alas, it was gone! But as had been the trend, I noticed another problem. I will be first to admit that of all the problems I had run across so far, this was the least significant. It did not even affect the functionality of the computer, but you have to realize, at this point I was like a guy that had his wife leave him and his dog hit by a car, only to have the cashier at McDonald's mess up his order (a stretch, but I stand by the analogy). As I typed on the keyboard, I noticed that the keys on the right side had a slightly different feel to them. The best way I can describe it is that it was like the laser engraving of the letters did not finish properly, and it left the symbols on the keys feeling a bit like sandpaper. It was only about a sixth of the keyboard, and you could hardly notice it while you were typing. But as they say, this was the straw that broke the camel's back. I wasn't about to have my laptop quest end with the most expensive option having an insanity-inducing tactile defect.

Chapter 4: A Mid-Midlife Crisis - 2/21/16
------------------------------
I would consider myself a fairly easy-going and optimistic person. I can't think of another time in my life where I have reached quite the level of stupefied disbelief and intellectual paralysis as I did on that day. Approximately two months had passed since I originally ordered a brand new laptop, and I still did not have a working machine. Was I naive to think that we live in a time when you can reliably buy new technology and expect it to work? A computer is the single most crucial tool for me to pursue my creative passions, and besides having two months of project development wisped away, here I sat with *no clue* on what I was going to do next.
<br>
<br>

![Image](/images/posts/2/indecision.png)

In my mind, things ultimately boiled down to three options:

### Option 1: Return the Surface Book, and Stick with Dell XPS 13
While I went through the process of attempting to buy a working Surface Book, I still had the Dell XPS 13 replacement system that I had already verified as working properly. The Dell XPS 13 was significantly cheaper than the Surface Book, and I had a hard time justifying the extra cost given the problems I had encountered so far. If I just returned the Surface Book, I could keep the Dell XPS 13 without having to worry about returning it to Dell (which, I would learn later, is not just painful but impossible). From a financial standpoint, this option seemed semi-attractive, though my feelings towards Dell were pretty shaky.

### Option 2: Get a Third Surface Book 
Maybe I had just been unlucky with the first two. Did I still feel that having a tablet-laptop crossover was useful enough to justify the extra cost? Honestly... not really. It was hard to believe that the extra ~$600 was delivering significant value, especially given the lack of reliability I had seen so far. The concept is cool, but every rational bone in my body had a hard time believing it was practical.

### Option 3: Jump Ship and Get a MacBook Pro
This thought had never even occurred to me until this mental breakdown. I had always used Windows machines because I was familiar with them and they had served me well over the years. What reason did I have to switch? Well, suddenly the last two months made a pretty strong case. Additionally, I started working at [ReadyTalk][readytalk] in January where I use a MacBook Pro as my primary computer. I had become more comfortable with OS X at work, and there was a strong case to be made about the advantages of crossover between tools used at work and at home. Plus, everyone I know that had used Macs would always use the same word:
<br>
<br>
"Reliable."
<br>
<br>
If there was one thing I craved more than anything at this point, it was a computer that would just work.
<br>
<br>

![Image](/images/posts/2/macbook_pro.jpg)

The longer I sat there in a daze, the more it seemed to become clear. I kept asking myself, "which option provides you with the tool that will enable you to pursue your passions most effectively?" I wanted this to be a very business-like decision, one that aligned with my long term aspirations. I couldn't deny the huge advantage that it would be to have the same setup at home as I did at work where I spent 40 hours of each week. Mac supports all of the crucial programs that I use, particularly Unity. In addition, it is a huge plus to have a Unix based terminal that allows for a much more enjoyable console experience, the importance of which was amplified by my decision to start learning [vim][vim] as my primary text editor. I couldn't really believe it, but I had decided to switch to Mac.
<br>

Chapter 5: My Third Trip to the Mall - 2/24/16
----------------------------------------------
There was something mildly comical about my route through the mall this time. I walked into the Microsoft store, returned the Surface Book, and then walked downstairs into the Apple store and bought a MacBook Pro. The lady working there was trying so hard to "help" me find the right computer, but I quickly cut her off and explained exactly the model that I wanted to buy.
<br>
<br>
"Sorry, I guess I am making your job too easy" I added.
<br>
<br>
I got home and repressed the aching memories as I removed yet another laptop from its packaging. And it worked. No surprises, just what a normal laptop-buying consumer would expect. I think deep down I kept expecting something to go wrong, but I slowly felt the peace of having put this journey to rest.
<br>

Chapter 6: My Fourth Trip to the Mall - 3/24/16
-----------------------------------------------
Wait, a fourth trip to the mall? Well, yes, it seems that the relentless onslaught of anger from the laptop gods was even too strong for the MacBook Pro. Having used it for about a month, one morning I tried to wipe a speck off the display. Except it didn't move. Turns out there was a dead pixel (or group of hyper-something pixels as I later was told) just off from the center of the screen. It was so small that I could only identify it about half of the time, but once you know it is there, there is no going back. At this point I was 100% convinced that the MacBook Pro was the right computer for me, so I figured, why not try out Apple's customer service while I'm at it?

### To the Genius Bar
I scheduled an appointment at the Genius Bar in the Apple store at the mall, and dragged Mary along with me the next day. We couldn't help but make fun of the situation as we walked into the mall for the fourth time. The Genius Bar was a pretty busy operation (there was a lady that walked in because her son had broken her iPhone screen literally while walking past the store), but we were able to get help in a reasonable amount of time with our reservation. If you ever want to feel like you are a crazy person, try explaining to a customer service employee that you are needing the screen fixed because of a dead pixel that they can't even see. Eventually, the poor guy gave me the benefit of a doubt (take notes Dell), and arranged for me to have the screen replaced. Conveniently, he said that they could ship it right back to my house to avoid another (fifth!) trip to the mall.
<br>
<br>

![Image](/images/posts/2/apple_store.png)

I received my MacBook Pro back in just three(!) days. I was pleasantly surprised, especially given that the outlet store needed to ship it to the actual repair facility first. But it was also probably because my mind was still running on Dell time. I had the unique scenario of experiencing the customer service of three different companies for addressing laptop issues. If I had to put them on a scale from 1-10 (10 being mind-blowingly good, 1 being "company turned out to be a scam") I would have to say Apple was a 9, Microsoft an 8, and Dell a 3. There were moments in my experience with Dell that I felt that they did a good job, but overall adjectives like "slow", "impersonal", and "frustrating" seem to dominate my experience; your mileage may vary. I am happy to say that this was the final stage in my quest and my MacBook Pro has been working flawlessly over the last 4 months. But speaking of Dell...
<br>

Chapter 7: Dell Puts a Nail in Their Own Coffin
-----------------------------------------------
While all of this Surface Book and MacBook Pro nonsense was taking place, I still had a replacement Dell XPS 13 in my possession. In fact, there was a moment where I had two Dell XPS 13's and a Surface Book all in my room, and I felt like some kind of weird hoarder, or the topic of that "Weird Addictions" TV show except instead of eating couch cushions I was addicted to dealing with tech customer service. As you may recall, the Dell agent had insisted that I verify the laptop was working (surprisingly, it was) before setting up a return. So once I had taken a brief look at it, I immediately contacted Dell about replacing it.

### I, the Buck, Get Passed
One of the most frustrating parts of my customer service experience with Dell was that besides the responses always being heartless, "cut and paste", templated excerpts, I also kept getting passed along to new agents. This was fine at first, but as the back story of my purchase got more and more lengthy and nuanced, it became very irritating to have to explain it all to someone who just saw my case for the first time. In order to return the system, I was told that I needed to just "go through customer support". I went ahead and filed a return request, waited and received the response that "I was outside of the return period". Are you kidding? They were measuring from the purchase *of the very first system*, before any of the shipping for repair and replacements which ate up the return period. So I called their customer service and unloaded this monologue about my frustrating experience and how it would be really helpful if I could just set up a return. To this the lady replied, "we can't, you are outside the return period." I explained how the only reason I was outside the return period was because of needing to ship it around due to problems with their product. It quickly became apparent that it was above the lady's pay grade to do anything about the situation. Exhausted from this whole process, the last thing I felt like doing was to escalate things to her manager, so I politely hung up.
<br>

### A Sunk Cost Partially Restored
As one last resort, I decided to tweet at Dell's support, but after my explanation, I simply got the same answer. It was very interesting to me how their support could see what objectively had been a terrible experience buying a new product from them, and still lack any empathy to try and help me in my situation. It is what it is, but I will be honest that it will be difficult for me to ever buy anything from Dell ever again. With no other choice, I turned to selling the Dell XPS 13 myself. Actually, I outsourced the selling to Mary's brother because my poor little soul couldn't handle anything more to do with laptop commerce. Months later it sold for around $300 less than my original purchase. At this point, that money was so long gone that it was like a nice little bonus.
<br>
<br>

![Image](/images/posts/2/mac_vs_pc.jpg)

Conclusion
----------
Writing this post reminded me of just how frustrating this whole experience was. All things considered, as weird as it sounds, I am kind of glad it all happened. Of course it was the kind of experience that fuels future nightmares, but at the same time, it may have been the only way I would have ended up with a Mac. Having now used a MacBook Pro at work and home over the last few months, it has become very clear that it is the right tool for me to be using. Had I gotten off the never-ending train of problems at an earlier stop, I would have a Dell XPS 13 or Surface Book, which I truly believe would have been a suboptimal choice for me. If you would have asked me nine months ago what it would take for me to switch to Mac, I would have said:
<br>
<br>
"A lot! Probably something crazy, I really have no reason to switch."
<br>
<br>
Well, a lot happened over the following months, it was pretty dang crazy, and I couldn't have been given a more persuasive reason to switch.

[dellxps13]: http://www.theverge.com/2015/12/7/9860958/dell-xps-13-laptop-review-2015
[readytalk]: https://www.readytalk.com/
[mary]: https://twitter.com/MarySorteberg
[witness]: http://store.steampowered.com/app/210970/
[vim]: https://en.wikipedia.org/wiki/Vim_(text_editor)
