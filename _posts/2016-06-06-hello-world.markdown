---
layout: post
title:  "Hello World!"
date:   2016-06-06
displaydate: June 6 2016
categories: website
---
Hey there! Somehow you stumbled across my first blog post, so thanks! Up to this point, my website has really just served as a place where I could put up all of my creative projects in one place. So why start a blog? My goal isn't to necessarily blog with any kind of regular cadence, but I wanted to have a platform where I could express my thoughts on topics I am passionate about in more depth than on [Twitter][twitter].  
<br>

[![Image](/images/posts/1/aerobatic.png)][aerobatic]

How I Made This Website for Free\*
---------------------------------
When looking into solutions for integrating a blog into my website, I eventually settled on [Jekyll][jekyll]. I was already hosting my site using [Bitbucket's][bitbucket] [Aerobatic][aerobatic], which [integrates with Jekyll quite nicely][jekyllwithaerobatic]. The primary reason for using Aerobatic was that I was already hosting my website's git repository through Bitbucket, and it offers free hosting! Not to mention, deploying my site is now as simple as pushing my changes to the repository, and Aerobatic automatically updates the live site. With this solution, the only remaining cost of my website is my domain name, which is a measly $10 a year.
<br>
<br>
Jekyll works as a static site generator. In other words, development of the site can be abstracted to an easier-to-use framework, and Jekyll takes care of generating the final static code that gets deployed. This allows the use of [Markdown][markdown] for blog posts and [Liquid][liquid] for more control flow options in the html, among other features. Combining this with Aerobatic makes things even more convenient. Rather than manually generating the static site using Jekyll and then uploading the results, Aerobatic handles it all. This keeps everything really clean since none of the final site files need to be included in the git repository. I just make my changes and push them to Bitbucket, Aerobatic uses Jekyll to generate the static site, and then Aerobatic deploys the results to the live site; really quite nice!
<br>
<br>

[![Image](/images/posts/1/jekyll.png)][jekyll]

I won't go much into the details of how to use Jekyll and Aerobatic since there are many great resources available (the sites for both [Jekyll][jekyll] and [Aerobatic][aerobatic] are clear and useful), but I'll quick highlight some of my favorite features of this particular setup:

### Writing Blog Posts in Markdown
One of the primary advantages of Jekyll is the ability to write blog posts in Markdown. While this did require me to learn some additional syntax (I hadn't really used Markdown before), it provides a really clean format for blog posts that works well with version control. This means I can write blog posts from the comfort of my text editor (read as: force me to get more comfortable with [Vim][vim]), with easy access to other parts of the site such as the CSS styling.

### Using Liquid Templating for Repeated Elements
My web design experience is fairly limited; it would be fair to say that this site was whipped together without huge concerns about code organization. That said, transitioning the site to Jekyll has helped significantly. For example, my [Games page][games_page] was previously an un-[DRY][dry] nightmare of repeated code. Using Liquid templating allows me to insert a loop directly into the html:

{% highlight html %}
{% raw %}
{% for game in site.data.games %}
  <div class="item">
    <div class="item_title">
      <table>
        <tr>
          <td>
            {% if game.title.size > 35 %}
              <h3>{{game.title}}</h3>
            {% else %}
              <h2>{{game.title}}</h2>
            {% endif %}
          </td>
          <td class="date">
            <p>{{game.date}}</p>
          </td>
        </tr>
      </table>
    </div>
    { ... }
  </div>
{% endfor %}
{% endraw %}
{% endhighlight %}

Now I don't have to put any of the game data directly in the html, but rather I can keep them in a separate data file that is loaded into the template using Liquid. There is a `games.yml` file that contains all of the actual game data, with each entry looking something like this:

{% highlight yml %}
- title: Introspection
  date: April 2015
  dev_time: 2 Days
  description: Made for <a href=http://ludumdare.com/compo/ludum-dare-32/?action=preview&uid=34830 target="_blank">Ludum Dare 32</a> with the theme,"An Unconventional Weapon." Fight off waves of negative thoughts, and capture positive thoughts to empower you in your fight against your doubts. Placed 56th in mood, 172nd in audio, and 219th overall out of nearly 1400 entries.
  link: http://gamejolt.com/games/introspection/130381
  screenshot: images/games/introspection.png
{% endhighlight %}

This makes it very easy to update site content without needing to mess with the actual templates. You will also notice that Liquid enables conditional logic, as is used in the above example to adjust the title's header size based on the length of the name.

### Pushing to Repo Instantly Deploys
I've already said it, but it is worth mentioning again just due to how convenient it is. I simply `git push` to my repository, and then nothing else! Aerobatic handles running Jekyll's build commands and deploying to the live site automatically. It even keeps a history of links to old deployments in case you ever need to view a previous version.

### Hosting is Free
Probably the biggest perk in my mind. I used to pay for hosting, but now the only cost associated with keeping my website online is the domain name, which is not even necessarily essential if you don't mind Aerobatic's default URL ([https://ryanlangewisch.aerobatic.io/][website_aerobatic] in my case).

Onward!
-------
While I enjoyed transitioning my website to use Jekyll, web development is not really what I enjoy working on most. Now that I have everything set up to enable blogging moving forward, I am excited to get moving on some game dev projects that have been on the backburner. Should you have any questions or comments, feel free to get in contact with me on [Twitter][twitter]!

[twitter]: https://twitter.com/rlangewi
[jekyll]: https://jekyllrb.com/ 
[bitbucket]: https://bitbucket.org/ 
[jekyllwithaerobatic]: https://www.aerobatic.com/blog/automated-continuous-deployment-of-jekyll-sites
[aerobatic]: https://www.aerobatic.com/
[markdown]: https://daringfireball.net/projects/markdown/
[liquid]: https://shopify.github.io/liquid/ 
[games_page]: /games.html 
[dry]: https://en.wikipedia.org/wiki/Don%27t_repeat_yourself
[website_aerobatic]: https://ryanlangewisch.aerobatic.io/
[vim]: https://en.wikipedia.org/wiki/Vim_(text_editor)
